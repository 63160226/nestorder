export class CreateCustomerDto {
  id: number;

  name: string;

  age: number;

  tel: string;

  gender: string;
}
